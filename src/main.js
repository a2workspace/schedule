import Vue from 'vue'
import App from './App.vue'
import i18n from './units/i18n'

require('./providers/componenets')

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  i18n,
}).$mount('#app')
