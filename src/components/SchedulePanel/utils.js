
/**
 * 取得不包含時間的 Date 物件
 *
 * @param  {Date} date
 * @return {Date}
 */
export function getDateWithoutTime(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}

/**
 * 取得該週第一天日期
 *
 * @param  {Date} date
 * @return {Date}
 */
export function getFirstDayOfWeek(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
}

/**
 * 取得該週最後一天日期
 *
 * @param  {Date} date
 * @return {Date}
 */
export function getLastDayOfWeek(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
}

/**
 * 取得下週第一天日期
 *
 * @param  {Date} date
 * @return {Date}
 */
export function getFirstDayOfNextWeek(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 7);
}

/**
 * 取得上週第一天日期
 *
 * @param  {Date} date
 * @return {Date}
 */
export function getFirstDayOfPreviousWeek(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() - 7);
}

/**
 * 取得當週的日期列表
 *
 * @param  {Date} date
 * @return {Array}
 */
export function getDateListOfThisWeek(date) {
  const firstDay = getFirstDayOfWeek(date);

  return Array(7)
    .fill(null)
    .map((n, dayOfWeek) => {
      return new Date(
        firstDay.getFullYear(),
        firstDay.getMonth(),
        firstDay.getDate() + dayOfWeek
      )
    })
}