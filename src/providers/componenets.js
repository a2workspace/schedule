import Vue from 'vue';

/**
 * =============================================================================
 * = 自動加載組件
 * =============================================================================
 *
 * 加載 components 目錄下的所有組件
 *
 */

const req = require.context('../components/', true, /\.(js|vue)$/i);

req.keys().forEach((key) => {
  let nameparts = key.split('/');

  // 處理名稱為 ./ComponentName.vue 格式之組件
  if (nameparts.length === 2) {
    return Vue.component(nameparts[1].match(/\w+/)[0], req(key).default);
  }

  // 處理名稱為 ./ComponentName/index.vue 格式之組件
  if (nameparts.length === 3) {
    if (nameparts[2].match(/index\./)) {
      return Vue.component(nameparts[1], req(key).default);
    }
  }
})