
module.exports = {
  weeks: {
    sun: 'Sun',
    mon: 'Mon',
    tue: 'Tue',
    wed: 'Wed',
    thu: 'Thu',
    fri: 'Fri',
    sat: 'Sat',
  },
  schedule: {
    'timezone-description': '* All the timings listed are in your timezone: {0} ({1})',
    'view-full-content': 'View full schedule',
  },
}